#coding=utf-8

import time
import urllib2
import cookielib

def make_cookie(name, value):
    return cookielib.Cookie(
        version=0,
        name=name,
        value=value,
        port=None,
        port_specified=False,
        domain="v2245.ants-city.com",
        domain_specified=True,
        domain_initial_dot=False,
        path="/",
        path_specified=True,
        secure=False,
        expires=1494780663,
        discard=False,
        comment=None,
        comment_url=None,
        rest=None
    )


#url = "http://v2245.ants-city.com/DRP/DRPFItemStockReport/GetData?action=read&page=1&start=0&limit=30&sort=[{%22property%22%3A%22Available%22%2C%22direction%22%3A%22DESC%22}]&filter=[{%22property%22%3A%22ItemInfo%22%2C%22value%22%3A%22111%22}]"
url = "http://v2245.ants-city.com/DRP/DRPFItemStockReport/GetData?_dc=1463039379259&action=read&page=1&start=0&limit=30&sort=%5B%7B%22property%22%3A%22Available%22%2C%22direction%22%3A%22DESC%22%7D%5D&filter=%5B%7B%22property%22%3A%22ItemInfo%22%2C%22value%22%3A%22N005%22%7D%5D"
#url = "http://127.0.0.1/testFile/require/cookie2.php"
#headers = {'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0','Referer' : 'http://v2245.ants-city.com/Account/Login?ReturnUrl=%2f'}

filename = 'cookie6.txt'

cookie = cookielib.MozillaCookieJar(filename)
try:
    #cookie.load(filename)
    #cookie.load(ignore_discard=True, ignore_expires=True)
    print(cookie)
except Exception as e:
    print e

cookie.set_cookie(make_cookie(".ASPXAUTH", "014E85394A6ED256BA94ED94DE255AF76FCC5455F512062A20A5120373BD7762C229A356B421C8FC24F10C11E28AC9C06CD5C707032CA422C3DAFEB11472EF0F5978117DE34851365B86B413F505B4B20A8336DAB2AEA7F15ABB55E04EDDB70364C57D725A981CF2B78C6B216F0CF661C739ADF2463D26D5A4B29041A0794C8D3E631880E5BB33C0C35954B841676ABB"))
cookie.set_cookie(make_cookie("ASP.NET_SessionId", "uecyvtzognnk42eay5twkpbg"))
cookie.set_cookie(make_cookie("__RequestVerificationToken", "fkbrXwtDYB7GbOTG8_7hHgVSE0-0Gk37fVqhFHBOZw6nmzlNeMLsbiwNpSwnVoUZMtpQI_otsfmzYYHYg9sea6BJpf0CNckwBCMcK_8wMpmtdjbGOc-2gw5wX1Wq_feIDiyxzrwX5j5J9Vrt3drOLg2"))


v = ''
for item in cookie:
    print(item.value)
    print(item.path)
    print(item.expires)
    print(item.name)



opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie))

try:
    result = opener.open(url)
    print result.read()
except Exception as e:
    print e


print('=============================')
#cookie.save(ignore_discard=True, ignore_expires=True)